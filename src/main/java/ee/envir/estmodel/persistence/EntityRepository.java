/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceUnit;
import jakarta.transaction.Transactional;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import java.util.List;

public abstract class EntityRepository<E> {

    private static final List<String> NULL_LIST = singletonList(null);

    @PersistenceUnit
    private EntityManagerFactory emf;

    public E find(String code) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var entity = em.find(this.getEntityClass(), code);

            if (entity == null) {
                throw new EntityNotFoundException();
            }

            return entity;

        }

    }

    public List<E> findAll(int limit, int offset, String code, String name, String... tags) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            return em.createNamedQuery(this.getEntityClass().getSimpleName()
                    + ".findAll", this.getEntityClass())
                    .setParameter("code", code)
                    .setParameter("name", name)
                    .setParameter("tags", tags.length > 0 ? asList(tags) : NULL_LIST)
                    .setParameter("count", tags.length)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();

        }

    }

    protected List<E> findBy(Class<?> parentClass, String parentCode,
            int limit, int offset, String code, String name, String... tags) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var entities = em.createNamedQuery(this.getEntityClass().getSimpleName()
                    + ".findBy" + parentClass.getSimpleName(), this.getEntityClass())
                    .setParameter(parentClass.getSimpleName().toLowerCase(), parentCode)
                    .setParameter("code", code)
                    .setParameter("name", name)
                    .setParameter("tags", tags.length > 0 ? asList(tags) : NULL_LIST)
                    .setParameter("count", tags.length)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();

            if (entities.isEmpty() && em.find(parentClass, parentCode) == null) {
                throw new EntityNotFoundException();
            }

            return entities;

        }

    }

    protected abstract Class<E> getEntityClass();

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Transactional
    public void remove(String code) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var entity = em.find(this.getEntityClass(), code);

            if (entity == null) {
                throw new EntityNotFoundException();
            }

            em.remove(entity);

        }

    }

}

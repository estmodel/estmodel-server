/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.EstModel;
import ee.envir.estmodel.persistence.entity.AssessmentUnit;
import ee.envir.estmodel.persistence.entity.AssessmentUnitModel;
import ee.envir.estmodel.persistence.entity.Country;
import ee.envir.estmodel.persistence.entity.CountryModel;
import ee.envir.estmodel.persistence.entity.District;
import ee.envir.estmodel.persistence.entity.DistrictModel;
import ee.envir.estmodel.persistence.entity.River;
import ee.envir.estmodel.persistence.entity.RiverModel;
import ee.envir.estmodel.persistence.entity.Station;
import ee.envir.estmodel.persistence.entity.StationModel;
import ee.envir.estmodel.persistence.entity.Subdistrict;
import ee.envir.estmodel.persistence.entity.SubdistrictModel;
import ee.envir.estmodel.persistence.entity.base.Catchment;
import ee.envir.estmodel.persistence.entity.base.Model;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceUnit;
import jakarta.transaction.Transactional;
import java.time.LocalDate;
import java.time.Year;
import java.util.List;

@ApplicationScoped
public class ModelRepository {

    @PersistenceUnit
    private EntityManagerFactory emf;

    private <M extends Model, C extends Catchment<M>> List<EstModel> findAll(
            Class<C> entityClass, String code, int startYear, int endYear) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            if (endYear == 0) {
                endYear = Year.now().getValue() - 1;
            }

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));

            return em.getReference(entityClass, code).getModels().stream()
                    .map(EstModel.class::cast).toList();

        }

    }

    public List<EstModel> findByAssessmentUnit(String code, int startYear, int endYear) {
        return this.findAll(AssessmentUnit.class, code, startYear, endYear);
    }

    public List<EstModel> findByCountry(String code, int startYear, int endYear) {
        return this.findAll(Country.class, code, startYear, endYear);
    }

    public List<EstModel> findByDistrict(String code, int startYear, int endYear) {
        return this.findAll(District.class, code, startYear, endYear);
    }

    public List<EstModel> findByRiver(String code, int startYear, int endYear) {
        return this.findAll(River.class, code, startYear, endYear);
    }

    public List<EstModel> findByStation(String code, int startYear, int endYear) {
        return this.findAll(Station.class, code, startYear, endYear);
    }

    public List<EstModel> findBySubdistrict(String code, int startYear, int endYear) {
        return this.findAll(Subdistrict.class, code, startYear, endYear);
    }

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    private <M extends Model, C extends Catchment<M>> void replaceAll(
            Class<C> entityClass, String code, int startYear, int endYear,
            List<M> models) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));

            em.createQuery("DELETE FROM " + entityClass.getSimpleName()
                    + "Model m WHERE m.code = :code")
                    .setParameter("code", code)
                    .executeUpdate();

            var entity = em.find(entityClass, code);

            if (entity == null) {
                throw new EntityNotFoundException();
            }

            models.forEach(EstModel::run);
            models.stream().distinct()
                    .filter(m -> code.equals(m.getCode())
                    && startYear <= m.getYear() && endYear >= m.getYear())
                    .forEach(em::persist);

        }

    }

    @Transactional
    public void replaceByAssessmentUnit(String code, int startYear, int endYear,
            List<EstModel> models) {

        this.replaceAll(AssessmentUnit.class, code, startYear, endYear, models.stream()
                .map(AssessmentUnitModel::new).toList());
    }

    @Transactional
    public void replaceByCountry(String code, int startYear, int endYear,
            List<EstModel> models) {

        this.replaceAll(Country.class, code, startYear, endYear, models.stream()
                .map(CountryModel::new).toList());
    }

    @Transactional
    public void replaceByDistrict(String code, int startYear, int endYear,
            List<EstModel> models) {

        this.replaceAll(District.class, code, startYear, endYear, models.stream()
                .map(DistrictModel::new).toList());
    }

    @Transactional
    public void replaceByRiver(String code, int startYear, int endYear,
            List<EstModel> models) {

        this.replaceAll(River.class, code, startYear, endYear, models.stream()
                .map(RiverModel::new).toList());
    }

    @Transactional
    public void replaceByStation(String code, int startYear, int endYear,
            List<EstModel> models) {

        this.replaceAll(Station.class, code, startYear, endYear, models.stream()
                .map(StationModel::new).toList());
    }

    @Transactional
    public void replaceBySubdistrict(String code, int startYear, int endYear,
            List<EstModel> models) {

        this.replaceAll(Subdistrict.class, code, startYear, endYear, models.stream()
                .map(SubdistrictModel::new).toList());
    }

}

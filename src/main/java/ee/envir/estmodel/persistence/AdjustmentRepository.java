/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.River;
import ee.envir.estmodel.persistence.entity.RiverAdjustment;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceUnit;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class AdjustmentRepository {

    @PersistenceUnit
    private EntityManagerFactory emf;

    public List<RiverAdjustment> findByRiver(String riverCode) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var adjustments = em.createNamedQuery("Adjustment.findByRiver", RiverAdjustment.class)
                    .setParameter("river", riverCode)
                    .getResultList();

            if (adjustments.isEmpty() && em.find(River.class, riverCode) == null) {
                throw new EntityNotFoundException();
            }

            return adjustments;

        }

    }

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Transactional
    public void replaceByRiver(String riverCode, Set<RiverAdjustment> adjustments) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.createNamedQuery("Adjustment.deleteByRiver")
                    .setParameter("river", riverCode)
                    .executeUpdate();

            var river = em.find(River.class, riverCode);

            if (river == null) {
                throw new EntityNotFoundException();
            }

            adjustments.forEach(l -> l.setRiver(river));
            adjustments.forEach(em::persist);

        }

    }

}

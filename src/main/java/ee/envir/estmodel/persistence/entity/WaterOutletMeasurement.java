/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.persistence.entity.base.Measurement;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import org.eclipse.persistence.annotations.PrimaryKey;

@Entity
@Table(name = "water_outlet_measurement")
@PrimaryKey(columns = {
    @Column(name = "water_outlet"),
    @Column(name = "\"parameter\""),
    @Column(name = "type"),
    @Column(name = "start_date")
})
@NamedQuery(name = "Measurement.deleteByWaterOutlet", query = """
    DELETE FROM WaterOutletMeasurement m
    WHERE :wateroutlet = m.code AND :type = m.type
    """)
@NamedQuery(name = "Measurement.findByWaterOutlet", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter,
    m.startDate, m.endDate, m.value, m.quantificationLimit,
    m.uncertainty, m.count, m.qualityFlag)
    FROM WaterOutletMeasurement m
    WHERE :wateroutlet = m.code AND :type = m.type
    ORDER BY m.parameter, m.startDate
    """)
@AttributeOverride(name = "code", column = @Column(name = "water_outlet", length = 15))
public class WaterOutletMeasurement extends Measurement {

    public void setWaterOutlet(WaterOutlet outlet) {
        this.code = outlet.getCode();
    }

}

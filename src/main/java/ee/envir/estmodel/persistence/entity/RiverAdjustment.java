/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.model.RiverBasin.Adjustment;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import static jakarta.persistence.FetchType.LAZY;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import org.eclipse.persistence.annotations.Cache;
import static org.eclipse.persistence.annotations.CacheType.SOFT;
import org.eclipse.persistence.annotations.PrimaryKey;

@Entity
@Table(name = "river_adjustment")
@PrimaryKey(columns = {
    @Column(name = "river"),
    @Column(name = "distance")
})
@NamedQuery(name = "Adjustment.deleteByRiver", query = """
    DELETE FROM RiverAdjustment a
    WHERE :river = a.river.code
    """)
@NamedQuery(name = "Adjustment.findByRiver", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.RiverAdjustment(a.area, a.overlapArea, a.calculationArea, a.distance, a.factor)
    FROM RiverAdjustment a
    WHERE :river = a.river.code
    ORDER BY a.distance
    """)
@Cache(type = SOFT)
@JsonbPropertyOrder({"area", "overlapArea", "calculationArea", "distance", "factor"})
public class RiverAdjustment implements Serializable, Adjustment {

    @Id
    @Column(name = "distance")
    private double distance;

    @Basic(optional = false)
    @Column(name = "area", nullable = false)
    private double area;

    @Basic(optional = false)
    @Column(name = "overlap_area", nullable = false)
    private double overlapArea;

    @Basic(optional = false)
    @Column(name = "calculation_area", nullable = false)
    private double calculationArea;

    @Basic(optional = false)
    @Column(name = "factor", nullable = false)
    private double factor;

    @JsonbTransient
    @ManyToOne(fetch = LAZY)
    @Id
    @JoinColumn(name = "river")
    private River river;

    public RiverAdjustment() {
    }

    public RiverAdjustment(double area, double overlapArea, double calculationArea, double distance, double factor) {
        this.area = area;
        this.overlapArea = overlapArea;
        this.calculationArea = calculationArea;
        this.distance = distance;
        this.factor = factor;
    }

    @Override
    public final boolean equals(Object object) {
        return object instanceof RiverAdjustment other
                && this.distance == other.distance
                && Objects.equals(this.river, other.river);
    }

    @Override
    public double getDistance() {
        return this.distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public double getArea() {
        return this.area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    @Override
    public double getOverlapArea() {
        return this.overlapArea;
    }

    public void setOverlapArea(double area) {
        this.overlapArea = area;
    }

    @Override
    public double getCalculationArea() {
        return this.calculationArea;
    }

    public void setCalculationArea(double area) {
        this.calculationArea = area;
    }

    @Override
    public double getFactor() {
        return this.factor;
    }

    public void setFactor(double factor) {
        this.factor = factor;
    }

    public River getRiver() {
        return this.river;
    }

    public void setRiver(River river) {

        if (this.getRiver() != null) {
            this.getRiver().getAdjustments().remove(this);
        }

        this.river = river;

        if (this.getRiver() != null) {
            this.getRiver().getAdjustments().add(this);
        }

    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.river, this.distance);
    }

}

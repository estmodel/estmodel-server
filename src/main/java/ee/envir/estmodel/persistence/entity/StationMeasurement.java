/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.persistence.entity.base.Measurement;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import org.eclipse.persistence.annotations.PrimaryKey;

@Entity
@Table(name = "station_measurement")
@PrimaryKey(columns = {
    @Column(name = "station"),
    @Column(name = "\"parameter\""),
    @Column(name = "type"),
    @Column(name = "start_date")
})
@NamedQuery(name = "Measurement.deleteByStation", query = """
    DELETE FROM StationMeasurement m
    WHERE :station = m.code AND :type = m.type
    """)
@NamedQuery(name = "Measurement.findMaxByStation", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter, m.startDate, m.endDate, m.value, m.count, m.qualityFlag)
    FROM StationMeasurement m, (SELECT m.parameter, MAX(m.value) maxValue
                                FROM StationMeasurement m
                                WHERE :station = m.code
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MAXIMUM = m.type
                                OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type)
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
                                GROUP BY m.parameter) m2
    WHERE :station = m.code
    AND m.parameter = m2.parameter
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MAXIMUM = m.type
    OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type)
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
    AND m.value = m2.maxValue
    ORDER BY m.parameter, m.startDate DESC
    """)
@NamedQuery(name = "Measurement.findMeanByStation", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter, MIN(m.startDate), MAX(m.startDate), AVG(CASE m.qualityFlag
    WHEN ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ THEN m.value * 0.5
    ELSE m.value END), SUM(m.count))
    FROM StationMeasurement m
    WHERE :station = m.code
    AND ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
    GROUP BY m.parameter
    ORDER BY m.parameter
    """)
@NamedQuery(name = "Measurement.findMinByStation", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter, m.startDate, m.endDate, m.value, m.count, m.qualityFlag)
    FROM StationMeasurement m, (SELECT m.parameter, MIN(m.value) minValue
                                FROM StationMeasurement m
                                WHERE :station = m.code
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type
                                OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MINIMUM = m.type)
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
                                GROUP BY m.parameter) m2
    WHERE :station = m.code
    AND m.parameter = m2.parameter
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type
    OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MINIMUM = m.type)
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
    AND m.value = m2.minValue
    ORDER BY m.parameter, m.startDate DESC
    """)
@NamedQuery(name = "Measurement.findMonthlyMaxByStation", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter, m.startDate, m.endDate, m.value, m.count, m.qualityFlag)
    FROM StationMeasurement m, (SELECT m.parameter, EXTRACT(MONTH FROM m.startDate) month, MAX(m.value) maxValue
                                FROM StationMeasurement m
                                WHERE :station = m.code
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MAXIMUM = m.type
                                OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type)
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
                                GROUP BY m.parameter, EXTRACT(MONTH FROM m.startDate)) m2
    WHERE :station = m.code
    AND m.parameter = m2.parameter
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MAXIMUM = m.type
    OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type)
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
    AND EXTRACT(MONTH FROM m.startDate) = m2.month
    AND m.value = m2.maxValue
    ORDER BY m.parameter, m.startDate DESC
    """)
@NamedQuery(name = "Measurement.findMonthlyMeanByStation", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter, MIN(m.startDate), MAX(m.startDate), AVG(CASE m.qualityFlag
    WHEN ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ THEN m.value * 0.5
    ELSE m.value END), SUM(m.count))
    FROM StationMeasurement m
    WHERE :station = m.code
    AND ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
    GROUP BY m.parameter, EXTRACT(MONTH FROM m.startDate)
    """)
@NamedQuery(name = "Measurement.findMonthlyMinByStation", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter, m.startDate, m.endDate, m.value, m.count, m.qualityFlag)
    FROM StationMeasurement m, (SELECT m.parameter, EXTRACT(MONTH FROM m.startDate) month, MIN(m.value) minValue
                                FROM StationMeasurement m
                                WHERE :station = m.code
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type
                                OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MINIMUM = m.type)
                                AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
                                GROUP BY m.parameter, EXTRACT(MONTH FROM m.startDate)) m2
    WHERE :station = m.code
    AND m.parameter = m2.parameter
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.Type.MEAN = m.type
    OR ee.envir.estmodel.persistence.entity.base.Measurement.Type.MINIMUM = m.type)
    AND (ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag.BELOW_LOQ = m.qualityFlag OR m.qualityFlag IS NULL)
    AND EXTRACT(MONTH FROM m.startDate) = m2.month
    AND m.value = m2.minValue
    ORDER BY m.parameter, m.startDate DESC
    """)
@NamedQuery(name = "Measurement.findByStation", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Measurement(m.parameter,
    m.startDate, m.endDate, m.value, m.quantificationLimit,
    m.uncertainty, m.count, m.qualityFlag)
    FROM StationMeasurement m
    WHERE :station = m.code AND :type = m.type
    ORDER BY m.parameter, m.startDate
    """)
@AttributeOverride(name = "code", column = @Column(name = "station", length = 15))
public class StationMeasurement extends Measurement {

    public void setStation(Station station) {
        this.code = station.getCode();
    }

}

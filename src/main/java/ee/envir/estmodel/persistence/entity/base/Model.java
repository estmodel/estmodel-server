/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity.base;

import ee.envir.estmodel.EstModel;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import java.util.List;
import java.util.Objects;
import org.eclipse.persistence.annotations.AdditionalCriteria;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.PrimaryKey;
import static org.eclipse.persistence.config.CacheIsolationType.ISOLATED;

@MappedSuperclass
@PrimaryKey(columns = {
    @Column(name = "code"),
    @Column(name = "\"year\"")
})
@AdditionalCriteria("""
    this.year >= CAST(SUBSTRING(:startDate, 1, 4) AS INT) AND 
    this.year <= CAST(SUBSTRING(:endDate, 1, 4) AS INT)
    """)
@Cache(isolation = ISOLATED)
@JsonbPropertyOrder({"id", "code", "name", "description", "year", "subcatchments"})
public abstract class Model extends EstModel {

    protected Model() {
    }

    protected Model(EstModel model) {
        this.setId(model.getId());
        this.setCode(model.getCode());
        this.setName(model.getName());
        this.setDescription(model.getDescription());
        this.setYear(model.getYear());
        this.setSubcatchments(model.getSubcatchments());
    }

    @Override
    public final boolean equals(Object object) {
        return object instanceof Model other
                && this.getYear() == other.getYear()
                && Objects.equals(this.getCode(), other.getCode());
    }

    @Override
    @Column(name = "id")
    public String getId() {
        return super.getId();
    }

    @Override
    @Id
    @Column(name = "code", length = 15)
    public String getCode() {
        return super.getCode();
    }

    @Override
    @Column(name = "name")
    public String getName() {
        return super.getName();
    }

    @Override
    @Column(name = "description", length = 4095)
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    @Id
    @Column(name = "\"year\"")
    public int getYear() {
        return super.getYear();
    }

    @Override
    @Column(name = "subcatchments")
    public List<EstModel.Subcatchment> getSubcatchments() {
        return super.getSubcatchments();
    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.getCode(), this.getYear());
    }

}

/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity.base;

import ee.envir.estmodel.model.RiverPoint;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.Positive;

@MappedSuperclass
@JsonbPropertyOrder({"overlapArea", "countryArea", "calculationArea", "distance"})
public abstract class Subcatchment<M extends Model> extends Catchment<M> implements RiverPoint {

    @Basic(optional = false)
    @Column(name = "overlap_area", nullable = false)
    private double overlapArea;

    @Basic(optional = false)
    @Column(name = "country_area", nullable = false)
    private double countryArea;

    @Basic(optional = false)
    @Column(name = "calculation_area", nullable = false)
    private double calculationArea;

    @Basic(optional = false)
    @Column(name = "distance", nullable = false)
    private double distance;

    protected Subcatchment() {
    }

    protected Subcatchment(String code) {
        super(code);
    }

    protected Subcatchment(String code, String name,
            double area, double overlapArea, double countryArea,
            double calculationArea, double distance) {

        super(code, name, area);
        this.overlapArea = overlapArea;
        this.countryArea = countryArea;
        this.calculationArea = calculationArea;
        this.distance = distance;
    }

    @Override
    public double getOverlapArea() {
        return this.overlapArea;
    }

    public void setOverlapArea(double area) {
        this.overlapArea = area;
    }

    @Positive
    public double getCountryArea() {
        return this.countryArea;
    }

    public void setCountryArea(double area) {
        this.countryArea = area;
    }

    @Override
    public double getCalculationArea() {
        return this.calculationArea;

    }

    public void setCalculationArea(double area) {
        this.calculationArea = area;
    }

    @Override
    public double getDistance() {
        return this.distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

}

/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.persistence.entity.base.Estimate;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import java.time.LocalDate;
import org.eclipse.persistence.annotations.PrimaryKey;

@Entity
@Table(name = "assessment_unit_estimate")
@PrimaryKey(columns = {
    @Column(name = "assessment_unit"),
    @Column(name = "\"parameter\""),
    @Column(name = "start_date")
})
@NamedQuery(name = "Estimate.deleteByAssessmentUnit", query = """
    DELETE FROM AssessmentUnitEstimate e
    WHERE e.code = :assessmentunit
    """)
@NamedQuery(name = "Estimate.findByAssessmentUnitGroupByP1D", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, e.startDate, e.endDate, e.value)
    FROM AssessmentUnitEstimate e
    WHERE e.code = :assessmentunit
    ORDER BY e.parameter, e.startDate
    """)
@NamedQuery(name = "Estimate.findByAssessmentUnitGroupByP1M", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, MIN(e.startDate), MAX(e.endDate),
    CASE e.parameter WHEN ee.envir.estmodel.persistence.entity.base.Estimate.Parameter.Q THEN AVG(e.value) ELSE SUM(e.value) END)
    FROM AssessmentUnitEstimate e
    WHERE e.code = :assessmentunit
    GROUP BY e.parameter, EXTRACT(YEAR FROM e.startDate), EXTRACT(MONTH FROM e.startDate)
    ORDER BY e.parameter, MIN(e.startDate)
    """)
@NamedQuery(name = "Estimate.findByAssessmentUnitGroupByP3M", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, MIN(e.startDate), MAX(e.endDate),
    CASE e.parameter WHEN ee.envir.estmodel.persistence.entity.base.Estimate.Parameter.Q THEN AVG(e.value) ELSE SUM(e.value) END)
    FROM AssessmentUnitEstimate e
    WHERE e.code = :assessmentunit
    GROUP BY e.parameter, EXTRACT(YEAR FROM e.startDate), EXTRACT(QUARTER FROM e.startDate)
    ORDER BY e.parameter, MIN(e.startDate)
    """)
@NamedQuery(name = "Estimate.findByAssessmentUnitGroupByP1Y", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, MIN(e.startDate), MAX(e.endDate),
    CASE e.parameter WHEN ee.envir.estmodel.persistence.entity.base.Estimate.Parameter.Q THEN AVG(e.value) ELSE SUM(e.value) END)
    FROM AssessmentUnitEstimate e
    WHERE e.code = :assessmentunit
    GROUP BY e.parameter, EXTRACT(YEAR FROM e.startDate)
    ORDER BY e.parameter, MIN(e.startDate)
    """)
@AttributeOverride(name = "code", column = @Column(name = "assessment_unit", length = 15))
public class AssessmentUnitEstimate extends Estimate {

    public AssessmentUnitEstimate() {
        super();
    }

    public AssessmentUnitEstimate(AssessmentUnit assessmentUnit, Parameter parameter, LocalDate date, double value) {
        super(assessmentUnit.getCode(), parameter, date, value);
    }

    public void setAssessmentUnit(AssessmentUnit assessmentUnit) {
        super.code = assessmentUnit.getCode();
    }

}

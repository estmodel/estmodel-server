/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.Country;
import ee.envir.estmodel.persistence.entity.District;
import ee.envir.estmodel.persistence.entity.River;
import ee.envir.estmodel.persistence.entity.Subdistrict;
import ee.envir.estmodel.persistence.entity.base.Estimate.Parameter;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import static java.lang.Double.max;
import static java.lang.Double.min;
import java.time.LocalDate;
import java.util.List;

@ApplicationScoped
public class RiverRepository extends EntityRepository<River> {

    public River find(String code, double distance, double area) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("parameters", List.of(Parameter.Q.name()));
            em.setProperty("startDate", LocalDate.EPOCH);
            em.setProperty("endDate", LocalDate.EPOCH);

            var river = em.find(this.getEntityClass(), code);

            if (river == null) {
                throw new EntityNotFoundException();
            }

            if (distance > 0) {

                if (area <= 0) {
                    area = river.area(distance);
                }

                double transboundryArea = river.getArea() - river.getCountryArea();
                double calculationArea = river.calculationArea(distance, area);

                em.detach(river);

                river.setArea(area);
                river.setOverlapArea(min(area, river.getOverlapArea()));
                river.setCountryArea(max(area - transboundryArea, 0));
                river.setCalculationArea(calculationArea);
                river.setLength(max(river.getLength() - distance, 0));

            }

            return river;

        }

    }

    public List<River> findByCountry(String countryCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(Country.class, countryCode,
                limit, offset, code, name, tags);
    }

    public List<River> findByDistrict(String districtCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(District.class, districtCode,
                limit, offset, code, name, tags);
    }

    public List<River> findByRiver(String riverCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(River.class, riverCode,
                limit, offset, code, name, tags);
    }

    public List<River> findBySubdistrict(String subdistrictCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(Subdistrict.class, subdistrictCode,
                limit, offset, code, name, tags);
    }

    @Override
    protected Class<River> getEntityClass() {
        return River.class;
    }

    @Override
    @Transactional
    public void remove(String code) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var river = em.find(this.getEntityClass(), code);

            if (river == null) {
                throw new EntityNotFoundException();
            }

            em.createNamedQuery("River.orphanByRiver")
                    .setParameter("river", code)
                    .executeUpdate();

            em.remove(river);

        }

    }

    @Transactional
    public void save(River river) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var district = river.getDistrict();

            if (district != null) {
                district = em.find(Subdistrict.class, district.getCode());
            }

            var parentRiver = river.getParentRiver().orElse(null);

            if (parentRiver != null) {
                parentRiver = em.find(River.class, parentRiver.getCode());
            }

            if (district == null && parentRiver == null) {
                throw new EntityNotFoundException();
            }

            var entity = em.find(this.getEntityClass(), river.getCode());

            if (entity != null) {
                entity.setName(river.getName());
                entity.setArea(river.getArea());
                entity.setOverlapArea(river.getOverlapArea());
                entity.setCountryArea(river.getCountryArea());
                entity.setCalculationArea(river.getCalculationArea());
                entity.setDistance(river.getDistance());
                entity.setLength(river.getLength());
                if (parentRiver != null) {
                    entity.setParentRiver(parentRiver);
                } else {
                    entity.setParentRiver(null);
                    entity.setDistrict(district);
                }
            } else {
                if (parentRiver != null) {
                    river.setParentRiver(parentRiver);
                } else {
                    river.setParentRiver(null);
                    river.setDistrict(district);
                }
                em.persist(river);
            }

        }

    }

}

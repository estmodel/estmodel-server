/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.AssessmentUnit;
import ee.envir.estmodel.persistence.entity.Country;
import ee.envir.estmodel.persistence.entity.District;
import ee.envir.estmodel.persistence.entity.River;
import ee.envir.estmodel.persistence.entity.Station;
import ee.envir.estmodel.persistence.entity.Subdistrict;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import ee.envir.estmodel.persistence.entity.base.Location;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceUnit;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class TagRepository {

    @PersistenceUnit
    private EntityManagerFactory emf;

    private <L extends Location> List<String> findBy(Class<L> entityClass, String code) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            return em.getReference(entityClass, code).getTags();

        }

    }

    public List<String> findByAssessmentUnit(String assessmentUnitCode) {
        return this.findBy(AssessmentUnit.class, assessmentUnitCode);
    }

    public List<String> findByCountry(String countryCode) {
        return this.findBy(Country.class, countryCode);
    }

    public List<String> findByDistrict(String districtCode) {

        return this.findBy(District.class, districtCode);
    }

    public List<String> findByRiver(String riverCode) {

        return this.findBy(River.class, riverCode);
    }

    public List<String> findByStation(String stationCode) {
        return this.findBy(Station.class, stationCode);
    }

    public List<String> findBySubdistrict(String subdistrictCode) {
        return this.findBy(Subdistrict.class, subdistrictCode);
    }

    public List<String> findByWaterOutlet(String outletCode) {
        return this.findBy(WaterOutlet.class, outletCode);
    }

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    private <L extends Location> void replaceBy(Class<L> entityClass, String code, Set<String> tags) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var entity = em.getReference(entityClass, code);
            entity.getTags().clear();
            entity.getTags().addAll(tags);

        }

    }

    @Transactional
    public void replaceByAssessmentUnit(String assessmentUnitCode, Set<String> tags) {
        this.replaceBy(AssessmentUnit.class, assessmentUnitCode, tags);
    }

    @Transactional
    public void replaceByCountry(String countryCode, Set<String> tags) {
        this.replaceBy(Country.class, countryCode, tags);
    }

    @Transactional
    public void replaceByDistrict(String districtCode, Set<String> tags) {
        this.replaceBy(District.class, districtCode, tags);
    }

    @Transactional
    public void replaceByRiver(String riverCode, Set<String> tags) {
        this.replaceBy(River.class, riverCode, tags);
    }

    @Transactional
    public void replaceByStation(String stationCode, Set<String> tags) {
        this.replaceBy(Station.class, stationCode, tags);
    }

    @Transactional
    public void replaceBySubdistrict(String subdistrictCode, Set<String> tags) {
        this.replaceBy(Subdistrict.class, subdistrictCode, tags);
    }

    @Transactional
    public void replaceByWaterOutlet(String outletCode, Set<String> tags) {
        this.replaceBy(WaterOutlet.class, outletCode, tags);
    }

}

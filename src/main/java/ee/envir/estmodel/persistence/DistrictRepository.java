/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.Country;
import ee.envir.estmodel.persistence.entity.District;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class DistrictRepository extends EntityRepository<District> {

    public List<District> findByCountry(String countryCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(Country.class, countryCode,
                limit, offset, code, name, tags);
    }

    @Override
    protected Class<District> getEntityClass() {
        return District.class;
    }

    @Transactional
    public void save(District district) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var country = em.find(Country.class, district.getCountry().getCode());

            if (country == null) {
                throw new EntityNotFoundException();
            }

            var entity = em.find(this.getEntityClass(), district.getCode());

            if (entity != null) {
                entity.setArea(district.getArea());
                entity.setName(district.getName());
                entity.setCountry(country);
            } else {
                district.setCountry(country);
                em.persist(district);
            }

        }

    }

}

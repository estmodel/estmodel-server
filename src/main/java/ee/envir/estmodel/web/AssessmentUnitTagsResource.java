/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.web;

import ee.envir.estmodel.persistence.TagRepository;
import io.helidon.security.annotations.Authenticated;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import java.util.Set;

@Singleton
@Path("assessment-units/{assessment-unit-code}/tags")
public class AssessmentUnitTagsResource {

    private final TagRepository repository;

    @Inject
    public AssessmentUnitTagsResource(TagRepository repository) {

        this.repository = repository;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getAssessmentUnitTags(
            @PathParam("assessment-unit-code") String assessmentUnitCode) {

        return this.repository.findByAssessmentUnit(assessmentUnitCode);

    }

    @Authenticated
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void setAssessmentUnitTags(
            @PathParam("assessment-unit-code") String assessmentUnitCode,
            @NotNull Set<@Size(max = 255) String> tags) {

        this.repository.replaceByAssessmentUnit(assessmentUnitCode, tags);

    }

}

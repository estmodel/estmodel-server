/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.web;

import ee.envir.estmodel.persistence.WaterOutletRepository;
import ee.envir.estmodel.persistence.entity.River;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import io.helidon.security.annotations.Authenticated;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Singleton
@Path("rivers/{river-code}/water-outlets")
public class RiverWaterOutletsResource {

    private final WaterOutletRepository repository;

    @Inject
    public RiverWaterOutletsResource(WaterOutletRepository repository) {

        this.repository = repository;

    }

    @Authenticated
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addRiverWaterOutlet(@PathParam("river-code") String riverCode,
            @Valid @NotNull WaterOutlet outlet) {

        outlet.setRiver(new River(riverCode));

        this.repository.save(outlet);

        return Response.created(URI.create("/water-outlets/" + outlet.getCode())).build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<WaterOutlet> getWaterOutletsByRiver(
            @PathParam("river-code") String riverCode,
            @QueryParam("limit") @PositiveOrZero int limit,
            @QueryParam("offset") @PositiveOrZero int offset,
            @QueryParam("code") String code,
            @QueryParam("name") String name,
            @QueryParam("tag") String... tags) {

        return this.repository.findByRiver(riverCode,
                limit, offset, code, name, tags);

    }

}

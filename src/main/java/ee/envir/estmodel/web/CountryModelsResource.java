/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.web;

import ee.envir.estmodel.EstModel;
import ee.envir.estmodel.persistence.ModelRepository;
import io.helidon.security.annotations.Authenticated;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Singleton
@Path("countries/{country-code}/models")
public class CountryModelsResource {

    private final ModelRepository repository;

    @Inject
    public CountryModelsResource(ModelRepository repository) {

        this.repository = repository;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<EstModel> getCountryModels(
            @PathParam("country-code") String countryCode,
            @QueryParam("start-year") int startYear,
            @QueryParam("end-year") int endYear) {

        return this.repository.findByCountry(countryCode, startYear, endYear);

    }

    @Authenticated
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void setCountryModels(
            @PathParam("country-code") String countryCode,
            @QueryParam("start-year") int startYear,
            @QueryParam("end-year") int endYear,
            @NotNull List<@Valid @NotNull EstModel> models) {

        this.repository.replaceByCountry(countryCode, startYear, endYear, models);

    }

}

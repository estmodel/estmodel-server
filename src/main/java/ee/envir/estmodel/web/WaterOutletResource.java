/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.web;

import ee.envir.estmodel.persistence.WaterOutletRepository;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import io.helidon.security.annotations.Authenticated;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Singleton
@Path("water-outlets/{water-outlet-code}")
public class WaterOutletResource {

    private final WaterOutletRepository repository;

    @Inject
    public WaterOutletResource(WaterOutletRepository repository) {

        this.repository = repository;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public WaterOutlet getWaterOutlet(@PathParam("water-outlet-code") String code) {

        return this.repository.find(code);

    }

    @Authenticated
    @DELETE
    public void removeWaterOutlet(@PathParam("water-outlet-code") String code) {

        this.repository.remove(code);

    }

}

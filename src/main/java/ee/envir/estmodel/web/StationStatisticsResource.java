/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.web;

import ee.envir.estmodel.persistence.StatisticRepository;
import ee.envir.estmodel.persistence.entity.base.Measurement.Parameter;
import ee.envir.estmodel.persistence.entity.base.Statistic;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.util.Collection;

@Singleton
@Path("stations/{station-code}/statistics")
public class StationStatisticsResource {

    private final StatisticRepository repository;

    @Inject
    public StationStatisticsResource(StatisticRepository repository) {

        this.repository = repository;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Statistic> getStationStatistics(
            @PathParam("station-code") String stationCode,
            @QueryParam("parameter") Parameter parameter,
            @QueryParam("start-year") int startYear,
            @QueryParam("end-year") int endYear) {

        return this.repository.findByStation(stationCode,
                parameter, startYear, endYear);

    }

}
